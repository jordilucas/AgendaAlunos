package br.com.jordilucas.persistenciasqlite.helper;

import br.com.jordilucas.persistenciasqlite.FormularioActivity;
import br.com.jordilucas.persistenciasqlite.R;
import br.com.jordilucas.persistenciasqlite.modelo.bean.Aluno;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;


public class FormularioHelper {

	private EditText nome;
	private EditText telefone;
	private EditText endereco;
	private EditText site;
	private EditText email;
	private EditText nota;
	private ImageView foto;
	
	private Aluno aluno;

	public FormularioHelper(FormularioActivity activity){
		
		nome = (EditText)activity.findViewById(R.id.edNome);
		telefone = (EditText)activity.findViewById(R.id.etFone);
		site = (EditText)activity.findViewById(R.id.etSite);
		email = (EditText)activity.findViewById(R.id.etEmail);
		endereco = (EditText)activity.findViewById(R.id.etEndereco);
		nota = (EditText)activity.findViewById(R.id.etNota);
		foto = (ImageView)activity.findViewById(R.id.ivFoto);
		
		aluno = new Aluno();
}
	
	public Aluno getAluno(){
		
		aluno.setNome(nome.getText().toString());
		aluno.setTelefone(telefone.getText().toString());
        aluno.setEndereco(endereco.getText().toString());
        aluno.setSite(site.getText().toString());
		aluno.setEmail(email.getText().toString());
		aluno.setNota(Double.valueOf(nota.getText().toString()));
		
		return aluno;
	}

    public void setAluno(Aluno aluno){

        nome.setText(aluno.getNome());
        telefone.setText(aluno.getTelefone());
        endereco.setText(aluno.getEndereco());
        site.setText(aluno.getSite());
        email.setText(aluno.getEmail());
        nota.setText(String.valueOf(aluno.getNota()));
        this.aluno = aluno;

        if(aluno.getFoto() !=null ){
            carregarFoto(aluno.getFoto());
        }

    }

    public void carregarFoto(String localFoto){

        Bitmap imagemFoto = BitmapFactory.decodeFile(localFoto);
        Bitmap imagemFotoReduzida = Bitmap.createScaledBitmap(imagemFoto, 100, 100, true);
        aluno.setFoto(localFoto);
        foto.setImageBitmap(imagemFotoReduzida);
    }


    public ImageView getFoto(){
        return foto;
    }


}


