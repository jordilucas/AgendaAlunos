package br.com.jordilucas.persistenciasqlite.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.jordilucas.persistenciasqlite.R;
import br.com.jordilucas.persistenciasqlite.modelo.bean.Aluno;

/**
 * Created by root on 14/05/14.
 */
public class ListaAlunoAdapter extends BaseAdapter {

    private final List<Aluno> listaAlunos;
    private final Activity activity;

    public ListaAlunoAdapter(Activity activity, List<Aluno> listaAlunos){
        this.listaAlunos = listaAlunos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listaAlunos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAlunos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaAlunos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.item, null);
        Aluno aluno = listaAlunos.get(position);

        if(position % 2 == 0){
            view.setBackgroundColor(activity.getResources().getColor(R.color.linha_par));
        }
        else{
            view.setBackgroundColor(activity.getResources().getColor(R.color.linha_impar));
        }

        TextView nome = (TextView)view.findViewById(R.id.itemNome);
        nome.setText(aluno.getNome());

        Bitmap bmp;

        if(aluno.getFoto() !=null){
            bmp = BitmapFactory.decodeFile(aluno.getFoto());
        }
        else{
            bmp = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_no_image);
        }

        bmp = Bitmap.createScaledBitmap(bmp, 100, 100, true);
        ImageView foto = (ImageView)view.findViewById(R.id.itemFoto);
        foto.setImageBitmap(bmp);

        return view;

    }
}
