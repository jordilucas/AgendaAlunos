package br.com.jordilucas.persistenciasqlite;


import java.util.List;

import br.com.jordilucas.persistenciasqlite.adapter.ListaAlunoAdapter;
import br.com.jordilucas.persistenciasqlite.modelo.bean.Aluno;
import br.com.jordilucas.persistenciasqlite.modelo.dao.AlunoDao;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;


public class ListaAlunosActivity extends Activity {
	
	private ListView lvListagem;
	
	private List<Aluno> listaAlunos;
	
	private Aluno alunoSelecionado = null;
	
	private ListaAlunoAdapter adapter;
	

	
	private final String TAG = "CADASTRO_ALUNO";
	//private final String ALUNOS_KEY = "LISTA";

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_alunos);
        
        lvListagem = (ListView)findViewById(R.id.lvListaAlunos);
        
        registerForContextMenu(lvListagem);
    
    
        lvListagem.setOnItemClickListener(new OnItemClickListener() {
		
        	
			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int posicao, long id) {

	                   Intent form = new Intent(ListaAlunosActivity.this,
                            FormularioActivity.class);

                       alunoSelecionado = (Aluno)lvListagem.getItemAtPosition(posicao);

                       form.putExtra("ALUNO_SELECIONADO", alunoSelecionado);

                        Log.i("TAG", "Aluno selecionado " + alunoSelecionado.getId());

                       startActivity(form);

			}


        });
    
        
        lvListagem.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					int posicao, long id) {
					
					alunoSelecionado = (Aluno) adapter.getItemAtPosition(posicao);
				
					Log.i(TAG, "Aluno selecionado ListView.longClick()"
							+ alunoSelecionado.getNome());
					
				return false;
			}
		});
        
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, 
    		ContextMenuInfo menuInfo){
    	
    		super.onCreateContextMenu(menu, view, menuInfo);
    	
    		getMenuInflater().inflate(R.menu.menu_contexto, menu);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item){
    	switch(item.getItemId()){
    		case R.id.menuDeletar:
                excluirAluno();
    			break;
            case R.id.menuLigar:
                intent = new Intent(Intent.ACTION_CALL);
    		    intent.setData(Uri.parse("tel: "+ alunoSelecionado.getTelefone()));
                startActivity(intent);
                break;
            case R.id.menuEnviarSMS:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("Tel: "+ alunoSelecionado.getTelefone()));
                startActivity(intent);
                break;
            case R.id.menuAcharNoMapa:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(("geo:0,0?z=14&q = "+ alunoSelecionado.getEndereco())));
                intent.putExtra("sms_body", "Mensagem de boas vindas");
                startActivity(intent);
                break;
            case R.id.menuNavegar:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http:"+alunoSelecionado.getSite()));
                startActivity(intent);
                break;
            case R.id.menuEnviarEmail:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{alunoSelecionado.getEmail()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Teste app");
                intent.putExtra(Intent.EXTRA_TEXT, "Corpo email");
                startActivity(intent);
                break;
            default:
    			break;
    	}
    	return super.onContextItemSelected(item);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        
    	MenuInflater inflater = this.getMenuInflater();
    	
    	inflater.inflate(R.menu.menu_principal, menu);
    	
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.menuNovo:
    		
    		startActivity(new Intent(this, FormularioActivity.class));
    		
    		return true;
    	
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
    
    
    public void carregarLista(){
    	AlunoDao dao = new AlunoDao(this);
    	
    	this.listaAlunos = dao.listar();
    	
    	dao.close();
    	
        this.adapter = new ListaAlunoAdapter(this, listaAlunos);
    	
    	this.lvListagem.setAdapter(adapter);
    	
    }
    
    @Override
    protected void onResume(){
    	super.onResume();
    	this.carregarLista();
    }
    
    private void excluirAluno(){
    	AlertDialog.Builder builder= new AlertDialog.Builder(this);
    	
    	builder.setMessage("Confirma a exclusão de: "
    			+alunoSelecionado.getNome());

        builder.setPositiveButton("sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlunoDao dao = new AlunoDao(ListaAlunosActivity.this);
                dao.deletar(alunoSelecionado);
                dao.close();
                carregarLista();
                alunoSelecionado = null;
            }
        });


    builder.setNegativeButton("Não", null);
    	AlertDialog dialog = builder.create();
    	
    	dialog.setTitle("Confirmação de operação");
    	dialog.show();
    }
    

    
    /*
    @Override
    protected void onSaveInstanceState(Bundle outState){
    	outState.putStringArrayList(ALUNOS_KEY, (ArrayList<String>) listaAlunos);
    	
    	super.onSaveInstanceState(outState);
    	
    	Log.i(TAG, "OnSavedInstanceState(): "+listaAlunos);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
    	super.onRestoreInstanceState(savedInstanceState);
    	
    	listaAlunos = savedInstanceState.getStringArrayList(ALUNOS_KEY);
    	
    	Log.i(TAG, "OnSaveRestoreState(): "+listaAlunos);
    }
   
    */
}
