package br.com.jordilucas.persistenciasqlite;

import br.com.jordilucas.persistenciasqlite.helper.FormularioHelper;
import br.com.jordilucas.persistenciasqlite.modelo.bean.Aluno;
import br.com.jordilucas.persistenciasqlite.modelo.dao.AlunoDao;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


import java.io.File;

public class FormularioActivity extends Activity {

    private Button botao;
    private FormularioHelper helper;
	
	//Variaveis para controle da camera
    private String localArquivo;
    private static final int FAZER_FOTO = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        Aluno alunoParaSerAlterado;
        helper = new FormularioHelper(this);
        botao = (Button)findViewById(R.id.btSalvar);

        helper.getFoto().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localArquivo = Environment.getExternalStorageDirectory()
                        + "/" + System.currentTimeMillis() + ".jpg";

                File arquivo = new File(localArquivo);

                Uri localFoto = Uri.fromFile(arquivo);

                Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);

                startActivityForResult(irParaCamera, FAZER_FOTO);


            }
        });

        alunoParaSerAlterado = (Aluno) getIntent().getSerializableExtra("ALUNO_SELECIONADO");

        if(alunoParaSerAlterado!=null){
            helper.setAluno(alunoParaSerAlterado);
        }

        botao.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Aluno aluno = helper.getAluno();
				
				AlunoDao dao = new AlunoDao(FormularioActivity.this);

                //if (String.valueOf(aluno.getId())==null) {
                    dao.cadastrar(aluno);

                //} else {
                  //  dao.alterar(aluno);
                //}
                dao.close();


			}
		});
        
        
    
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	
    	MenuInflater inflater  = this.getMenuInflater();
    	
    	inflater.inflate(R.menu.menu_principal, menu);


    	return true;
    	
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.menuNovo:
    		
    		startActivity(new Intent(this, ListaAlunosActivity.class));
    		
    		return true;

    	
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == FAZER_FOTO){
            if(resultCode == Activity.RESULT_OK){
                helper.carregarFoto(this.localArquivo);
            }
            else{
                localArquivo = null;
            }
        }

    }
}
